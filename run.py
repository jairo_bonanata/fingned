#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask, request, abort
from calcular_similitud import procesar_datos
from flask_socketio import SocketIO, emit

app = Flask(__name__)
socketio = SocketIO(app)



@app.route('/desambiguar', methods=['POST'])
def desambiguar():
    if not request.json or 'ruta' not in request.json:
        abort(400)
    procesar_datos(request.json['ruta'])
    return 'todo salio bien', 201

if __name__ == '__main__':
    socketio.run(app)
