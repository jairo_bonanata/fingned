# coding=utf-8
import mysql.connector
import datetime

config = {
    'user': 'root',
    'password': '',
    'host': '127.0.0.1',
    'database': 'wikipedia_propio',
    'raise_on_warnings': False,
}


def ejecutar_consulta_desde_archivo_sql(ruta_sql):
    print datetime.datetime.now()
    archivo = open(ruta_sql, 'r')
    sql = " ".join(archivo.readlines())  # traigo lo que voy a ejecutar
    archivo.close()
    conn = mysql.connector.connect(**config)  # Conectar a la base de datos
    cursor = conn.cursor()  # Crear un cursor
    for query in sql.split(';')[0:-1]:
        cursor.execute(query)
    conn.commit()  # Hacer efectiva la escritura de datos
    cursor.close()  # Cerrar el cursor
    conn.close()  # Cerrar la conexion
    print datetime.datetime.now()


def run_query(query, dato):
    conn = mysql.connector.connect(**config)  # Conectar a la base de datos
    cursor = conn.cursor()  # Crear un cursor
    cursor.execute(query, dato)
    columnas_cursor = cursor.fetchall()
    cursor.close()  # Cerrar el cursor
    conn.close()  # Cerrar la conexion
    return columnas_cursor


def run_query2(queries, datos):
    conn = mysql.connector.connect(**config)  # Conectar a la base de datos
    cursor = conn.cursor()  # Crear un cursor
    res = []
    i = 0
    for dato in datos:
        res.append([])
        for query in queries:
            cursor.execute(query, (dato,))
            res[i] = res[i] + cursor.fetchall()
        i += 1
    cursor.close()  # Cerrar el cursor
    conn.close()  # Cerrar la conexion
    return res
