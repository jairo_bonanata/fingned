#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
from nltk.stem.snowball import SnowballStemmer
from cgi import escape
import os
import datetime
import re
from sklearn.metrics.pairwise import linear_kernel
from sklearn.feature_extraction import DictVectorizer
import lxml.etree as etree
from ConexionMySQL import run_query, run_query2


# obtener entidades con nombre a desambiguar dentro del documento
def obtener_candidatos(fuente):
    # ir a la base de datos y traer las entidades con esta surface_form
    # si no esta en el coso asumimos que no es un nombre valido es decir no es una entidad con nombre
    consultasql = "SELECT articulo FROM mapeo_sf_entidad WHERE nombre=%s"
    resultado = run_query(consultasql, (fuente,))
    resultado_unicode = []
    for dato in resultado:
        resultado_unicode.append(dato[0].decode('utf-8'))
    return resultado_unicode


# obtener entidades con nombre a desambiguar dentro del documento
def obtener_sf(entidad):
    # ir a la base de datos y traer las entidades con esta surface_form
    # si no esta en el coso asumimos que no es un nombre valido es decir no es una entidad con nombre
    consultasql = "SELECT nombre FROM mapeo_sf_entidad WHERE articulo=%s"
    resultado = run_query(consultasql, (entidad,))
    resultado_unicode = []
    for dato in resultado:
        resultado_unicode.append(dato[0].decode('utf-8'))
    return resultado_unicode


# obtener los contextos posibles del documento accediendo a la base de datos, de esta forma puedo armar un vector
def obtener_contextos_posibles(candidatos):
    if not candidatos:
        return {}
    consultas = list()
    consultas.append("SELECT contexto FROM contextos WHERE articulo=%s")
    consultas.append("SELECT destino FROM links WHERE origen=%s")
    consultas.append("SELECT origen FROM links WHERE destino=%s")
    resultado = run_query2(consultas, candidatos)
    resultado_unicode = {}
    for dato in resultado:
        for dato2 in dato:
            resultado_unicode[dato2[0].decode('utf-8')] = 1
    return resultado_unicode


# tendria que tener un preprocesado de los datos...
# el preprocesado deberia ser tanto para los contextos como para
# el texto en si mismo
def normalizar_datos(buff):
    buff.lower()  # no se que mas hacer...
    raise NotImplementedError('normalizar_datos no esta implementada')


# cuento cuantas veces aparece cada termino en el texto
def armar_diccionario_terminos(texto, contextos):
    d1 = {}
    for termino in contextos:
        contador = 0
        for sf in obtener_sf(termino):
            contador += texto.count(sf)
        d1[termino] = contador
    return d1
    # return {termino: texto.count(obtener_sf(termino)) for termino in contextos}


def leer_archivo_freeling():
    f = open('/home/jairo/buscopiniones/opiniones/entrada', 'r')
    lineas = f.readlines()
    lineas = [linea.decode('utf-8').strip('\n') for linea in lineas]
    l = [linea.split(' ') for linea in lineas]
    f.close()
    return l


def leer_archivo_freeling2():
    f = open('/home/jairo/PycharmProjects/wikiparser/xml/entradas/salidaFreeling.txt', 'r')
    archivos = f.read().split('-------------------------------------------------------------- '
                              '-------------------------------------------------------------- Fz 1')
    f.close()
    lineas = []
    for archivo in archivos:
        lineas.append([linea.split(' ')[0:3] for linea in archivo.decode('utf-8').splitlines()])
    return lineas


def obtener_texto(m):
    l = []
    for x in m:
        l.append(x[0])
    return l


def obtener_nombres(l):
    d = {}
    for i, x in enumerate(l):
        if len(x) > 2 and x[2] == 'NP00000':
            candidatos_i = obtener_candidatos(x[0].replace('_', ' ').lower())
            if candidatos_i:
                d[i] = obtener_candidatos(x[0].replace('_', ' ').lower())
    return d


def obtener_contextos(d):
    contextos = []
    d2 = {}
    for i in d:
        contextos += obtener_contextos_posibles(d[i])
        d2[i] = [obtener_contextos_posibles([x]) for x in d[i]]
    return set(contextos), d2


def desambiguar(diccionario, contextos_posta):
    mejores = {}
    for i in contextos_posta[1]:
        v = DictVectorizer().fit(contextos_posta[1][i] + [diccionario])
        x = v.transform(contextos_posta[1][i])
        y = v.transform(diccionario)
        mejores[i] = linear_kernel(x, y).argmax()
    return mejores


def taggear_archivo(matriz_freeling):
    print datetime.datetime.now()
    tokens = obtener_texto(matriz_freeling)
    texto_unicode = ' '.join(tokens).lower()
    nombres = obtener_nombres(matriz_freeling)
    contextos_posta = obtener_contextos(nombres)
    diccionario = armar_diccionario_terminos(texto_unicode, contextos_posta[0])
    desambiguados = desambiguar(diccionario, contextos_posta)
    for I, _ in enumerate(tokens):
        if I in nombres:
            tokens[I] = u'\r\n<entidad id="{0}">\r\n'.format(escape(nombres[I][desambiguados[I]], quote=True)) \
                        + escape(tokens[I].replace('_', ' '), quote=True) + u'</entidad>\r\n'
        else:
            tokens[I] = escape(tokens[I].replace('_', ' '), quote=True)
    texto_taggeado = '<?xml version="1.0" encoding="utf-8"?>\r\n'
    texto_taggeado += '<add>\r\n'
    texto_taggeado += ' '.join(tokens)
    texto_taggeado += '</add>'
    ruta = os.curdir + os.sep + 'xml' + os.sep + 'taggeado' + os.sep + 'taggeado'
    arch_salida = open(ruta + datetime.datetime.now().strftime('%H%M%S%f')[:-5] + '.xml', 'w')
    arch_salida.write(texto_taggeado.encode('utf-8'))
    arch_salida.close()
    print datetime.datetime.now()


def taggear_archivos(matrices_freeling):
    for matriz_freeling in matrices_freeling:
        taggear_archivo(matriz_freeling)
