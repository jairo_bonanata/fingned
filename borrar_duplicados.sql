use wikipedia_propio;

create table articulos2 like articulos;
ALTER TABLE articulos ADD INDEX index1 (titulo ASC);
insert articulos2 select * from articulos group by titulo;

create table categoria_articulo2 like categoria_articulo;
ALTER TABLE categoria_articulo ADD INDEX index1 (categoria ASC, articulo ASC);
insert categoria_articulo2 select * from categoria_articulo group by categoria, articulo;

create table contextos2 like contextos;
ALTER TABLE contextos ADD INDEX index1 (articulo ASC, contexto ASC);
insert contextos2 select * from contextos group by articulo, contexto;

CREATE TABLE links2 (
  `origen` VARCHAR(60) NULL,
  `destino` VARCHAR(60) NULL,
  `cantidad` INT NULL);
ALTER TABLE links ADD INDEX index1 (origen ASC, destino ASC);
insert links2 select origen, destino, COUNT(*) from links group by origen, destino;

CREATE TABLE mapeo_sf_entidad2 (
  `articulo` VARCHAR(60) NULL,
  `nombre` VARCHAR(60) NULL,
  `cantidad` INT NULL);
ALTER TABLE mapeo_sf_entidad ADD INDEX index1 (articulo ASC, nombre ASC);
insert mapeo_sf_entidad2 select articulo, nombre, COUNT(*) from mapeo_sf_entidad group by articulo, nombre;

create table redirecciones2 like redirecciones;
ALTER TABLE redirecciones ADD INDEX index1 (origen ASC, destino ASC);

insert redirecciones2 SELECT r1.origen as origen, IFNULL(r2.destino, r1.destino) as destino
FROM redirecciones r1
LEFT JOIN redirecciones r2 ON r1.destino = r2.origen;


DROP TABLE articulos;
DROP TABLE categoria_articulo;
DROP TABLE contextos;
DROP TABLE links;
DROP TABLE mapeo_sf_entidad;
DROP TABLE redirecciones;

RENAME TABLE articulos2 TO articulos;
RENAME TABLE categoria_articulo2 TO categoria_articulo;
RENAME TABLE contextos2 TO contextos;
RENAME TABLE links2 TO links;
RENAME TABLE mapeo_sf_entidad2 TO mapeo_sf_entidad;
RENAME TABLE redirecciones2 TO redirecciones;

ALTER TABLE articulos ADD INDEX index1 (titulo ASC);
ALTER TABLE categoria_articulo ADD INDEX index1 (categoria ASC);
ALTER TABLE contextos ADD INDEX index1 (articulo ASC);
ALTER TABLE links ADD INDEX index1 (origen ASC);
ALTER TABLE mapeo_sf_entidad ADD INDEX index1 (articulo ASC);
ALTER TABLE redirecciones ADD INDEX index1 (origen ASC);

ALTER TABLE categoria_articulo ADD INDEX index2 (articulo ASC);
ALTER TABLE contextos ADD INDEX index2 (contexto ASC);
ALTER TABLE links ADD INDEX index2 (destino ASC);
ALTER TABLE mapeo_sf_entidad ADD INDEX index2 (nombre ASC);
ALTER TABLE redirecciones ADD INDEX index2 (destino ASC);

DELETE FROM contextos WHERE contextos.articulo NOT IN (SELECT titulo FROM articulos);
DELETE FROM redirecciones WHERE redirecciones.destino NOT IN (SELECT titulo FROM articulos);
DELETE FROM links WHERE links.origen NOT IN (SELECT titulo FROM articulos);
DELETE FROM links WHERE links.destino NOT IN (SELECT titulo FROM articulos);
DELETE FROM mapeo_sf_entidad WHERE mapeo_sf_entidad.articulo NOT IN (SELECT titulo FROM articulos);
DELETE FROM categoria_articulo WHERE categoria_articulo.articulo NOT IN (SELECT titulo FROM articulos);

ALTER TABLE articulos ADD CONSTRAINT pk_titulo PRIMARY KEY (titulo);
