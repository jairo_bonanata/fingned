#!/usr/bin/python
# -*- coding: utf-8 -*-
import socket
import sys
import struct
from thread import *
from desambiguador_solr import procesar_datos

 
HOST = ''    # Symbolic name meaning all available interfaces
PORT = 5000  # Arbitrary non-privileged port
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created'
 
# Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
     
print 'Socket bind complete'
 
# Start listening on socket
s.listen(5)
print 'Socket now listening'


# Function for handling connections. This will be used to create threads
def atender(conn):
    xml = ""
    largo = conn.recv(4)
    a_leer = struct.unpack('!i', largo)[0]
    print a_leer
    conn.sendall(struct.pack('!i', a_leer))
    print a_leer
    bytes_leidos = 0
    while bytes_leidos < a_leer:
        data = conn.recv(min(a_leer - bytes_leidos, 8192))
        bytes_leidos += len(data)
        xml += data
    respuesta = procesar_datos(xml)
    conn.sendall(struct.pack('!i', len(respuesta)))
    conn.sendall(respuesta)
    conn.close()


# now keep talking with the client
while 1:
    # wait to accept a connection - blocking call
    conn_, addr = s.accept()
    print 'Connected with ' + addr[0] + ':' + str(addr[1])
    start_new_thread(atender, (conn_,))

s.close()
