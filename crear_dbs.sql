DROP SCHEMA `wikipedia_propio`;
CREATE SCHEMA `wikipedia_propio` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;

CREATE TABLE `wikipedia_propio`.`categoria_articulo` (
  `categoria` VARCHAR(60) NULL,
  `articulo` VARCHAR(60) NULL);

CREATE TABLE `wikipedia_propio`.`contextos` (
  `articulo` VARCHAR(60) NULL,
  `contexto` VARCHAR(60) NULL);

CREATE TABLE `wikipedia_propio`.`links` (
  `origen` VARCHAR(60) NULL,
  `destino` VARCHAR(60) NULL);

CREATE TABLE `wikipedia_propio`.`mapeo_sf_entidad` (
  `articulo` VARCHAR(60) NULL,
  `nombre` VARCHAR(60) NULL);

CREATE TABLE `wikipedia_propio`.`articulos` (
  `titulo` VARCHAR(60) NOT NULL);

CREATE TABLE `wikipedia_propio`.`redirecciones` (
  `destino` VARCHAR(60) NULL,
  `origen` VARCHAR(60) NULL);
