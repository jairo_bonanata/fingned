#!/usr/bin/python
# -*- coding: utf-8 -*-

from nltk.stem.snowball import SnowballStemmer
import re
from sklearn.metrics.pairwise import linear_kernel
from sklearn.feature_extraction import DictVectorizer
import lxml.etree as etree
from ConexionSolr import realizar_consulta, realizar_consulta2


def procesar_datos(xml):
    stemmer = SnowballStemmer("spanish")
    root = etree.fromstring(xml)
    d = {}
    for nombre in root.findall('.//n'):
        clave = stemmer.stem(nombre.text.lower())
        if clave in d:
            d[clave] += 1
        else:
            d[clave] = 1
    for grupo in root.findall(".//opinion"):
        fuente = grupo.find("./fuente")
        if fuente is not None:
            nombre = fuente.xpath("n[starts-with(@tag, 'NP')]")
            # print "entre"
            if nombre:
                # print "entre"
                entidad = desambiguar_solr(d, nombre[0].text, stemmer)
                if entidad is not None:
                    fuente_desambiguada = etree.Element("fuente_desambiguada", entidad=entidad)
                    grupo.append(fuente_desambiguada)
    quitar_tags = re.compile('</?n.*?>', re.IGNORECASE | re.UNICODE | re.DOTALL)
    return quitar_tags.sub('', etree.tostring(root))


def desambiguar_solr(d, nombre, stemmer):
    results = realizar_consulta2(nombre, d.keys())
    # print results
    # v = DictVectorizer(sparse=False)
    # x = v.fit_transform(d)
    # diccionarios = []
    if not results:
        return None
    # for doc in results:
    #     tmp = {}
    #     for key in d:
    #         tmp[key] = 0
    #     for termino in (doc['links_entrantes'] + doc['links_salientes']):
        #   print coso
        #   term = stemmer.stem(termino.lower())
        #   if term in d:
        #   tmp[termino] = 1
        #   print (doc['links_entrantes'] + doc['links_salientes'])
        #   diccionarios.append(tmp)
    # y = v.transform(diccionarios)
    # mejor = linear_kernel(x, y).argmax()
    return results[0]['titulo']
