LOAD DATA INFILE '/home/jairo/PycharmProjects/wikiparser/csv/categoria_articulo.csv' 
	INTO TABLE  `wikipedia_propio`.`categoria_articulo`
	CHARACTER SET utf8
    FIELDS 
		TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"'
		ESCAPED BY '"'
	LINES TERMINATED BY '\r\n';

LOAD DATA INFILE '/home/jairo/PycharmProjects/wikiparser/csv/contextos.csv' 
	INTO TABLE  `wikipedia_propio`.`contextos`
	CHARACTER SET utf8
    FIELDS 
		TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"'
		ESCAPED BY '"'
	LINES TERMINATED BY '\r\n';

LOAD DATA INFILE '/home/jairo/PycharmProjects/wikiparser/csv/links.csv' 
	INTO TABLE `wikipedia_propio`.`links`
	CHARACTER SET utf8
    FIELDS 
		TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"'
		ESCAPED BY '"'
	LINES TERMINATED BY '\r\n';

LOAD DATA INFILE '/home/jairo/PycharmProjects/wikiparser/csv/mapeo_sf_entidad.csv' 
	INTO TABLE `wikipedia_propio`.`mapeo_sf_entidad`
	CHARACTER SET utf8
    FIELDS 
		TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"'
		ESCAPED BY '"'
	LINES TERMINATED BY '\r\n';

LOAD DATA INFILE '/home/jairo/PycharmProjects/wikiparser/csv/redirecciones.csv' 
	INTO TABLE `wikipedia_propio`.`redirecciones`
	CHARACTER SET utf8
    FIELDS 
		TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"'
		ESCAPED BY '"'
	LINES TERMINATED BY '\r\n';

LOAD DATA INFILE '/home/jairo/PycharmProjects/wikiparser/csv/articulos.csv' 
	INTO TABLE `wikipedia_propio`.`articulos`
	CHARACTER SET utf8
    FIELDS 
		TERMINATED BY ','
		OPTIONALLY ENCLOSED BY '"'
		ESCAPED BY '"'
	LINES TERMINATED BY '\r\n';
