#!/usr/bin/python
# -*- coding: utf-8 -*-
from cgi import escape
import csv
import os
import re
from lxml import etree
from nltk.corpus import stopwords
from WikiExtractor import clean, dropNested
from ConexionMySQL import ejecutar_consulta_desde_archivo_sql, run_query
import datetime


# expresion regular global para poder extraer el titulo
regex = re.compile("([\w\s\-]+?)[\s]*?\(([\w]+[\w\s\-]+?)\)", re.IGNORECASE | re.UNICODE | re.DOTALL)


# extraer nombre del titulo
def extraer_sf(titulo):
    res = regex.findall(titulo)
    if len(res) > 0:
        return map(unicode, res[0])
    else:
        return None


stopwords_set = set([unicode(palabra) for palabra in stopwords.words('spanish')])


def descartable(s):
    try:
        float(s)
        return True
    except ValueError:
        if len(s) < 3 or len(s) > 60:
            return True
        else:
            return s in stopwords_set


def descartable_tupla(t):
    return True in [descartable(s) for s in t]


def escribir_utf8(writer, row):
    row = [s.encode("utf-8") for s in row]
    writer.writerow(row)


def primer_parrafo(texto_wiki):
    resultado = texto_wiki.lstrip().split('\n\n')
    return resultado[0]


def generar_csv_s():
    # infile = "prueba2.xml"
    infile = "/home/jairo/Descargas/eswiki-latest-pages-articles.xml"
    context = etree.iterparse(infile, events=('end',), tag='page', encoding='utf-8')
    print datetime.datetime.now()
    # funcion lambda para filtrar las tuplas a guardar en los archivos
    nodescartable = lambda t: not descartable_tupla(t)
    minusculas_todo = lambda l: [tuple((s.lower() for s in t)) for t in l]
    # expresion regular para extraer los links internos de wikipedia
    links_con_pipe_re = re.compile("\[\[[\s]*([\s\(\)\w\-]*?[\w])[\s]*?\|[\s]*([\w]+[\s\(\)\w\-]*?[\w])[\s]*?\]\]",
                                   re.IGNORECASE | re.UNICODE | re.DOTALL)
    # expresion regular para extraer los links internos de wikipedia sin pipe
    links_sin_pipe_re = re.compile("\[\[[\s]*([\s\(\)\w\-]*?[\w])[\s]*?\]\]", re.IGNORECASE | re.UNICODE | re.DOTALL)
    # expresion regular para saber si una pagina es de desambiguacion
    desam_regex = re.compile("\{\{desambiguaci[oó]n\}\}".decode('utf-8'), re.IGNORECASE | re.UNICODE | re.DOTALL)
    categ_regex = re.compile("\[\[Categor[ií]a\:([\s\(\)\w\-]*)[\s]*?\|?[\s\(\)\w\-]*?\]\]".decode('utf-8'),
                             re.IGNORECASE | re.UNICODE | re.DOTALL)
    links_archivos = re.compile("\[\[([\s\(\)\w\-]*)\:([\s\(\)\w\-]*)[\s]*?\|*?[\s\(\)\w\-]*?\]\]".decode('utf-8'),
                                re.IGNORECASE | re.UNICODE | re.DOTALL)
    # abro los archivos csv antes de procesar todo
    archivo_links = open(os.curdir + os.sep + "csv" + os.sep + "links.csv", "w")
    archivo_contextos = open(os.curdir + os.sep + "csv" + os.sep + "contextos.csv", "w")
    archivo_categoria_articulo = open(os.curdir + os.sep + "csv" + os.sep + "categoria_articulo.csv", "w")
    archivo_mapeo_sf = open(os.curdir + os.sep + "csv" + os.sep + "mapeo_sf_entidad.csv", "w")
    archivo_redirect = open(os.curdir + os.sep + "csv" + os.sep + "redirecciones.csv", "w")
    archivo_articulo = open(os.curdir + os.sep + "csv" + os.sep + "articulos.csv", "w")
    # los que escriben los archivos csv
    writer_links = csv.writer(archivo_links)
    writer_contextos = csv.writer(archivo_contextos)
    writer_categoria_articulo = csv.writer(archivo_categoria_articulo)
    writer_mapeo_sf = csv.writer(archivo_mapeo_sf)
    writer_redirect = csv.writer(archivo_redirect)
    writer_articulos = csv.writer(archivo_articulo)
    # por cada elemento del artículo de wikipedia
    for event, elem in context:
        ns = elem.find("ns")
        redirect = elem.find("redirect")
        # hay un redirect y no es una pagina de wikipedia especial
        if redirect is not None:
            # filas_mapeo_sf = []
            filas_redirect = []
            if ns.text == "0":
                titulo = elem.find("title")
                clave = unicode(redirect.get("title"))
                titulo_str = unicode(titulo.text)
                dato = (clave, titulo_str)
                # filas_mapeo_sf.append(dato)
                filas_redirect.append(dato)
                titulo.clear()
            redirect.clear()
            # filas_mapeo_sf = filter(nodescartable, minusculas_todo(filas_mapeo_sf))
            filas_redirect = filter(nodescartable, minusculas_todo(filas_redirect))
            # for val in filas_mapeo_sf:
            #    escribir_utf8(writer_mapeo_sf, val)
            for val in filas_redirect:
                escribir_utf8(writer_redirect, val)
            del filas_redirect
            # del filas_mapeo_sf
        # no es una pagina de wikipedia especial ni tampoco un redirect
        elif ns.text == "0":
            filas_mapeo_sf = []
            filas_links = []
            filas_contextos = []
            filas_categoria_articulo = []
            # obtengo el titulo y el texto
            titulo = elem.find("title")
            texto = elem.find("revision/text")
            # me aseguro que todo sea unicode
            texto_text = unicode(texto.text)
            titulo_str = unicode(titulo.text).lower()
            # lo necesito afuera por si esta pertenece a desambiguacion, extraigo categorias
            categorias_art = map(unicode, categ_regex.findall(texto_text))
            # me fijo si la categoria desambiguacion no pertenece al articulo, se cumple para todos que es none
            # todos estos chequeos son para saber si no es una pagina de desambiguacion
            bandera_desambiguacion = 'desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= 'desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= 'Desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= 'Desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= desam_regex.search(texto_text) is None
            # con expresiones regulares extraigo los links internos de wikipedia (los con pipe par saber los mapeos)
            links_con_pipe = [(unicode(x), unicode(y)) for (x, y) in links_con_pipe_re.findall(texto_text)]
            for entidad, nombre in links_con_pipe:
                dato = (entidad, nombre)
                filas_mapeo_sf.append(dato)
            # tiro los templates, tablas, etc
            texto_text = dropNested(texto_text, r'{{', r'}}')
            texto_text = dropNested(texto_text, r'{\|', r'\|}')
            texto_text = links_archivos.sub('', texto_text)
            texto_text = primer_parrafo(texto_text)
            # no es pagina de desambiguacion
            if bandera_desambiguacion and not descartable(titulo_str):
                # extraigo los links nuevamente
                links_sin_pipe = map(unicode, links_sin_pipe_re.findall(texto_text))
                links_con_pipe = [(unicode(x), unicode(y)) for (x, y) in links_con_pipe_re.findall(texto_text)]
                # si no es desambiguacion tengo que guardar el titulo porque es una entidad
                writer_articulos.writerow((titulo_str.encode('utf-8'),))
                for entidad, nombre in links_con_pipe:
                    dato = (titulo_str, entidad)
                    filas_links.append(dato)
                for entidad in links_sin_pipe:
                    dato = (titulo_str, entidad)
                    filas_links.append(dato)
                for categoria in categorias_art:
                    dato = (categoria, titulo_str)
                    filas_categoria_articulo.append(dato)
                # extraigo la surface form desde el titulo cosas con parentesis
                sf_contexto = extraer_sf(titulo_str)
                if sf_contexto is not None:
                    sf = sf_contexto[0]
                    contexto = sf_contexto[1]
                    dato = (titulo_str, contexto)
                    filas_contextos.append(dato)
                    dato = (titulo_str, sf)
                    filas_mapeo_sf.append(dato)
                else:
                    dato = (titulo_str, titulo_str)
                    filas_mapeo_sf.append(dato)
                del categorias_art
                del links_sin_pipe
            else:
                for entidad, nombre in links_con_pipe:
                    dato = (entidad, nombre)
                    filas_mapeo_sf.append(dato)
            titulo.clear()
            texto.clear()
            filas_links = filter(nodescartable, minusculas_todo(filas_links))
            filas_contextos = filter(nodescartable, minusculas_todo(filas_contextos))
            filas_categoria_articulo = filter(nodescartable, minusculas_todo(filas_categoria_articulo))
            filas_mapeo_sf = filter(nodescartable, minusculas_todo(filas_mapeo_sf))
            for val in filas_links:
                escribir_utf8(writer_links, val)
            for val in filas_contextos:
                escribir_utf8(writer_contextos, val)
            for val in filas_categoria_articulo:
                escribir_utf8(writer_categoria_articulo, val)
            for val in filas_mapeo_sf:
                escribir_utf8(writer_mapeo_sf, val)
            del filas_links
            del filas_contextos
            del filas_categoria_articulo
            del filas_mapeo_sf
            del links_con_pipe
        ns.clear()
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context
    # cierro los archivos csv luego de procesar todo
    archivo_links.close()
    archivo_contextos.close()
    archivo_categoria_articulo.close()
    archivo_mapeo_sf.close()
    archivo_redirect.close()
    archivo_articulo.close()
    print datetime.datetime.now()


# funcion que imprime el nodo xml escapeado tambien comillas dobles
def elemento_xml(tag, texto):
    return '<field name="' + tag + '">' + escape(texto, quote=True) + '</field>\r\n'


# funcion que imprime el diccionario y genera un nodo xml, para un campo multivaluado de solr
def lista_xml(tag, lista):
    if not lista:
        return '<field name="' + tag + '"></field>\r\n'
    else:
        resultado = ''
        for elemento in lista:
            resultado += elemento_xml(tag, elemento)
        return resultado


def consultar_columna(base, campor, campoq, articulo):
    consultasql = "SELECT {0} FROM {1} WHERE {2}=%s".format(campor.encode('utf-8'), base.encode('utf-8'),
                                                            campoq.encode('utf-8'))
    resultado = run_query(consultasql, (articulo,))
    resultado_unicode = []
    for dato in resultado:
        resultado_unicode.append(dato[0].decode('utf-8'))
    return resultado_unicode


def armar_archivo_solr():
    identificador = 0
    infile = "/home/jairo/Descargas/eswiki-latest-pages-articles.xml"
    context = etree.iterparse(infile, events=('end',), tag='page', encoding='utf-8')
    print datetime.datetime.now()
    desam_regex = re.compile("\{\{desambiguaci[oó]n\}\}".decode('utf-8'), re.IGNORECASE | re.UNICODE | re.DOTALL)
    categ_regex = re.compile("\[\[Categor[ií]a\:([\s\(\)\w\-]*)[\s]*?\|?[\s\(\)\w\-]*?\]\]".decode('utf-8'),
                             re.IGNORECASE | re.UNICODE | re.DOTALL)
    links_archivos = re.compile("\[\[([\s\(\)\w\-]*)\:([\s\(\)\w\-]*)[\s]*?\|*?[\s\(\)\w\-]*?\]\]".decode('utf-8'),
                                re.IGNORECASE | re.UNICODE | re.DOTALL)
    # por cada elemento del artículo de wikipedia
    ruta = os.curdir + os.sep + 'xml' + os.sep + 'solr' + os.sep + 'indizarSolr'
    f = open(ruta + str(identificador) + '.xml', 'w')
    f.write('<?xml version="1.0" encoding="utf-8"?>\r\n')
    f.write('<add>\r\n')
    bytes_escritos = 0
    for event, elem in context:
        ns = elem.find("ns")
        redirect = elem.find("redirect")
        # hay un redirect y no es una pagina de wikipedia especial
        if redirect is not None:
            redirect.clear()
        # no es una pagina de wikipedia especial ni tampoco un redirect
        elif ns.text == "0":
            # obtengo el titulo y el texto
            titulo = elem.find("title")
            texto = elem.find("revision/text")
            texto_text = unicode(texto.text)
            titulo_str = unicode(titulo.text).lower()
            # lo necesito afuera por si esta pertenece a desambiguacion, extraigo categorias
            categorias_art = map(unicode, categ_regex.findall(texto_text))
            # me fijo si la categoria desambiguacion no pertenece al articulo, se cumple para todos que es none
            # todos estor chequeos son para saber si no es una pagina de desambiguacion
            bandera_desambiguacion = 'desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= 'desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= 'Desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= 'Desambiguación'.decode('utf-8') not in categorias_art
            bandera_desambiguacion &= desam_regex.search(texto_text) is None
            # no es pagina de desambiguacion
            if bandera_desambiguacion and not descartable(titulo_str):
                texto_text = dropNested(texto_text, r'{{', r'}}')
                texto_text = dropNested(texto_text, r'{\|', r'\|}')
                texto_text = links_archivos.sub('', texto_text)
                texto_text = primer_parrafo(texto_text)
                # aca esta todo el jugo papa
                buff = '<doc>\r\n'.encode('utf-8')
                buff += elemento_xml('titulo', titulo_str).encode('utf-8')
                buff += elemento_xml('texto', clean(texto_text)).encode('utf-8')
                buff += lista_xml('links_entrantes',
                                  consultar_columna('links', 'origen', 'destino', titulo_str)).encode('utf-8')
                buff += lista_xml('links_salientes',
                                  consultar_columna('links', 'destino', 'origen', titulo_str)).encode('utf-8')
                buff += lista_xml('surface_forms',
                                  consultar_columna('mapeo_sf_entidad', 'nombre', 'articulo', titulo_str)).encode(
                    'utf-8')
                buff += lista_xml('categorias',
                                  consultar_columna('categoria_articulo', 'categoria', 'articulo', titulo_str)).encode(
                    'utf-8')
                buff += lista_xml('contextos',
                                  consultar_columna('contextos', 'contexto', 'articulo', titulo_str)).encode('utf-8')
                buff += '</doc>\r\n'.encode('utf-8')
                # no puede pesar demasiado la salida, limite de 500MB
                if len(buff) + bytes_escritos > 536870912:
                    bytes_escritos = 0
                    identificador += 1
                    f.write('</add>')
                    f.close()
                    f = open(ruta + str(identificador) + '.xml', 'w')
                    f.write('<?xml version="1.0" encoding="utf-8"?>\r\n')
                    f.write('<add>\r\n')
                bytes_escritos += len(buff)
                f.write(buff)
            titulo.clear()
            texto.clear()
        ns.clear()
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    f.write(os.linesep)
    f.write('</add>')
    f.close()
    del context
    print datetime.datetime.now()

generar_csv_s()
ejecutar_consulta_desde_archivo_sql("crear_dbs.sql")
ejecutar_consulta_desde_archivo_sql("cargar_dbs.sql")
ejecutar_consulta_desde_archivo_sql("borrar_duplicados.sql")
armar_archivo_solr()
