#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib2
import simplejson as json


# obtengo los candidatos
def realizar_consulta(sf):
    datos = {"query": "*:*",
             "filter": "surface_forms:" + '"' + sf + '"',
             "fields": ["titulo", "links_entrantes", "links_salientes", "categorias"],
             "limit": "10"}
    url = 'http://localhost:8983/solr/wikipedia/query'
    req = urllib2.Request(url, data=json.dumps(datos))
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req)
    json_data = json.loads(response.read())
    return json_data['response']['docs']

def realizar_consulta2(sf, nombres):
    query = " ".join(nombres)
    datos = {"query": query,
             "filter": "surface_forms:" + '"' + sf + '"',
             "fields": ["titulo", "links_entrantes", "links_salientes", "categorias"],
             "limit": "1"}
    url = 'http://localhost:8983/solr/wikipedia/query'
    req = urllib2.Request(url, data=json.dumps(datos))
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req)
    json_data = json.loads(response.read())
    print json_data
    return json_data['response']['docs']

# def realizar_consulta2(docs):
#     # tengo que eliminar los links vacíos
#     doc = docs[0]
#     print doc['titulo']
#     print doc['links_entrantes']
#     links = set(doc['links_entrantes'] + doc['links_salientes'])
#     iterlinks = iter(links)
#     filtro = 'titulo:' + next(iterlinks)
#     print links
#     for link in iterlinks:
#         filtro += " OR titulo:" + link
#     print filtro
#     datos = {"query": "*:*",
#              "filter": filtro,
#              "fields": ["titulo", "surface_forms"]}
#     print datos
#     url = 'http://localhost:8983/solr/wikipedia/query'
#     req = urllib2.Request(url, data=json.dumps(datos))
#     req.add_header('Content-Type', 'application/json')
#     response = urllib2.urlopen(req)
#     json_data = json.loads(response.read())
#     # print json_data
#     for doc in json_data['response']['docs']:
#         print doc['surface_forms']
#

# realizar_consulta2(realizar_consulta("andorra"))

# def realizar_consulta3():
#     datos = {"query":  "{!child of=doctype:articulo doScores=true}"
#                        "{!edismax qf=articulo pf}",
#              "fields": ["*", "score"]}
#     url = 'http://localhost:8983/solr/buscopiniones/query'
#     req = urllib2.Request(url, data=json.dumps(datos))
#     req.add_header('Content-Type', 'application/json')
#     response = urllib2.urlopen(req)
#     json_data = json.loads(response.read())
#     print json_data['response']['docs']

# realizar_consulta3()
# from nltk.corpus import WordNetCorpusReader
# wncr = WordNetCorpusReader('wordnet_spa')
